module Graphics.DemoCat.CairoSDL ( createSurfacePair ) where

import           Text.Printf ( printf )

import           Data.Vector.Storable.Mutable as VSM ( IOVector )
import           Data.Word ( Word8 )

import           GHC.Ptr ( castPtr )
import           Linear ( V2 (..)
                        , _x
                        , _y )

import           Foreign.C.String ( peekCString )
import           Foreign.C ( CInt (CInt) )

import qualified Graphics.Rendering.Cairo
    as C ( Format (FormatRGB24, FormatARGB32)
         , Surface
         , createImageSurfaceForData )

import qualified SDL
    as S ( Surface
         , PixelFormat (ARGB4444, RGB888, RGBA8888, ARGB8888, BGR888)
         , createRGBSurface
         , createRGBSurfaceFrom
         , loadBMP
         , lockSurface
         , unlockSurface
         , surfacePixels
         , surfaceDimensions )

import           Graphics.DemoCat.SDLUtil ( checkSDLError )

createCSFromSS :: S.Surface -> IO C.Surface
createCSFromSS surf = do
    checkSDLError "dimensions"
    dimensions <- S.surfaceDimensions surf
    let (width', height') = wh' dimensions
        stride' = 4 * width'
        wh' (V2 x y) = (fromIntegral x, fromIntegral y)
    putStrLn . printf "width: %d height: %d stride: %d" width' height' $ stride'
    checkSDLError "lockSurface"
    S.lockSurface surf
    checkSDLError "getPixels"
    pixelData <- castPtr <$> S.surfacePixels surf
    checkSDLError "createCairo"
    C.createImageSurfaceForData
        pixelData C.FormatARGB32 width' height' stride'

createSurfacePair :: Bool -> IO (S.Surface, C.Surface)
createSurfacePair doBg = do
    -- | sdl surface: no transparency -- simpler to paste cairo surface,
    -- whose alpha pixels are pre-multiplied, straight in.
    sdlSurf <- makeSdlSurf' doBg
    cairoSurf <- createCSFromSS sdlSurf
    checkSDLError "create cairo surf"
    pure (sdlSurf, cairoSurf) where
        makeSdlSurf' False = S.createRGBSurface (V2 800 600) S.ARGB8888
        makeSdlSurf' True = S.loadBMP "bandits.bmp"
