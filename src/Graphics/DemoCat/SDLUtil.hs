module Graphics.DemoCat.SDLUtil ( checkSDLError ) where

import           Text.Printf ( printf )
import           Foreign.C.String ( peekCString )
import qualified SDL.Raw.Error as SRE ( getError
                                      , clearError )
import           System.IO as I ( hPutStrLn, stderr )

checkSDLError :: String -> IO ()
checkSDLError tag = do
    check' =<< peekCString =<< SRE.getError
    SRE.clearError where
        check' "" = pure ()
        check' theError = I.hPutStrLn I.stderr theError
