module Graphics.DemoCat.Render.Util
    ( anyChar
    , toLowerString
    , toUpperString
    , upperFirst
    , trim
    , digitsToInt
    , charRange, charRangeFrom, charRangeTo
    , numCharRange, numCharRangeFrom, numCharRangeTo
    , slurp
    , oneOfIgnoreCase
    , stringIgnoreCase
    , sp
    , sps
    , varId, varIdCh, varId1Ch
    , alphaCh, alphaLowerCh, alphaUpperCh
    ) where

import           Data.Char
    ( isSpace, toUpper, toLower, digitToInt, chr, ord )

import           Text.ParserCombinators.Parsec
    ( GenParser, string, oneOf, try, many, satisfy, (<|>) )

type Parser = GenParser Char ()

upperFirst :: String -> String
upperFirst [] = []
upperFirst (x:xs) = toUpper x : xs

toLowerString :: String -> String
toLowerString [] = ""
toLowerString (x:xs) = toLower x : toLowerString xs

toUpperString :: String -> String
toUpperString [] = ""
toUpperString (x:xs) = toUpper x : toUpperString xs

-- | not efficient.
trim :: String -> String
trim = reverse . trim' . reverse . trim' where
    trim' "" = ""
    trim' (x:xs)
      | isSpace x = trim xs
      | otherwise = x:xs

digitsToInt :: String -> Int
digitsToInt "" = error "digitsToInt: empty string"
digitsToInt s = digitsToInt' (0 :: Double) (reverse s) where
    digitsToInt' _ [] = 0
    digitsToInt' m (x:xs) =
        digitToInt x * (floor $ 10 ** m) + digitsToInt' (m + 1) xs

slurp :: String -> IO String
slurp = readFile

charRange :: Char -> Char -> Maybe [Char]
charRange l r
  | l > r = Nothing
  | otherwise = Just $ map chr [ord l .. ord r]

charRangeFrom :: Char -> Int -> [Char]
charRangeFrom l n = map chr [ordl .. ordl + n - 1] where
    ordl = ord l

charRangeTo :: Char -> Int -> [Char]
charRangeTo r n = map chr [ordr - n + 1 .. ordr] where
    ordr = ord r

-- | e.g. a0-a5, a0-, -a5
numCharRange :: Char -> Int -> Int -> Maybe [String]
numCharRange c nl nr
  | nl > nr = Nothing
  | otherwise = Just $ map ((c:) . show) [nl .. nr]

numCharRangeFrom :: Char -> Int -> Int -> [String]
numCharRangeFrom c nl num = map ((c:) . show) [nl .. nl + num - 1]

numCharRangeTo :: Char -> Int -> Int -> [String]
numCharRangeTo c nr num = map ((c:) . show) [nr - num + 1 .. nr]

---- | parsers.

oneOfIgnoreCase :: String -> Parser Char
oneOfIgnoreCase str = oneOf $ toLowerString str ++ toUpperString str

stringIgnoreCase :: String -> Parser String
stringIgnoreCase str = str' l' <|> str' u' where
    l' = toLowerString str
    u' = toUpperString str
    str' = try . string

alphaCh'l :: [Char]
alphaCh'l = "abcdefghijklmnopqrstuvwxyz"

alphaCh'u :: [Char]
alphaCh'u = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

numCh' :: [Char]
numCh' = "0123456789"

sp :: Parser Char
sp = oneOf " \t\n"

-- | zero or more.
sps :: Parser String
sps = many sp

anyChar :: Parser Char
anyChar = satisfy $ const True

alphaCh :: Parser Char
alphaCh = oneOf $ alphaCh'l ++ alphaCh'u

alphaLowerCh :: Parser Char
alphaLowerCh = oneOf $ alphaCh'l

alphaUpperCh :: Parser Char
alphaUpperCh = oneOf $ alphaCh'u

varId :: Parser String
varId = (:) <$> varId1Ch <*> many varIdCh

varId1Ch :: Parser Char
varId1Ch = oneOf $ "_" ++ alphaCh'l

varIdCh :: Parser Char
varIdCh = oneOf $ "_'" ++ alphaCh'l ++ alphaCh'u ++ numCh'

